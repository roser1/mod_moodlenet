This file is part of Moodle - http://moodle.org/

Moodle is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Moodle is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

copyright  2019 Mayel de Borniol  {@link http://mayel.space}
license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later


MoodleNet module
=============

MoodleNet module is one of the successors to original 'file' type plugin of Resource module.

Icon credit
============
<a href="https://icons8.com/icon/11815/add-file">Add File icon by Icons8</a>
