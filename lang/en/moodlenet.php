<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'url', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    mod_moodlenet
 * @copyright  2019 Mayel de Borniol  {@link http://mayel.space}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['modulename'] = 'MoodleNet';
$string['pluginname'] = 'MoodleNet';
$string['searchmoodlenet'] = 'Search MoodleNet content';
$string['modulenameplural'] = 'MoodleNet Resources';
$string['search:activity'] = 'From MoodleNet';
$string['modulename_help'] = 'This enables a teacher to search MoodleNet when adding a new course resource or activity. Anything that is freely available online and curated by educators can be found on MoodleNet.
                                Note that the resource may be imported as any kind of Moodle activity or resource, depending on what format the content is in.';
$string['modulename_link'] = 'mod/moodlenet';
$string['pluginadministration'] = 'MoodleNet resource module administration';
$string['moodlenet:addinstance'] = 'Add a new MoodleNet resource';
$string['moodlenet:view'] = 'View MoodleNet resource';
$string['downloadingresource'] = 'Downloading the resource';
$string['downloadingmsg'] = 'Please wait, the resource is being downloaded and analysed...';
$string['downloadconfirmed'] = $string['downloaded'] = 'The resource has been downloaded in a temporary file area as {$a}';
$string['preview_embed'] = "Preview of the content that will be embeded as a Page resource:";
$string['preview_html'] = "Preview of the content that will be added as a Page resource:";
$string['domainurl'] = "Domain URL";
$string['addresourcetocourse'] = "Add this resource to my course";
$string['errormovefile'] = "Error: could not move the downloaded file.";
$string['resourcecomesfrom'] = "This resource comes from an ";
$string['externalsource'] = "external source ";
$string['via'] = "via a ";
$string['moodlenetcollection'] = "MoodleNet collection";
$string['errorfetchingresource'] = 'Error fetching the resource: {$a}';
$string['errorprocessingwebpage'] = 'Error processing webpage text: {$a}';
$string['error'] = 'Error: {$a}';
$string['import_resources'] = 'Import resources from MoodleNet';
$string['import_resources_help'] = 'Instructions:

* Type relevant keywords or phrases to describe your topic in the MoodelNet search box.
* The search result will show different communities, collections and resources as per your search keyword.
* <b>To preview</b> a resource, click on the thumbnail of resource.
* <b>To import</b> a resource, click on the \'To Moodle\' button.';
$string['import_resource'] = 'Import a resource';
$string['import_resource_help'] = '
* If the resulting resource type is \'Page\', you can preview the content on this page.
* If the resultingresource type is \'File\', you can see details in next page.
* If the resulting resource type is \'URL\', you can see details in next page.
* If the resource is a Moodle course backup, the page automatically redirects to the Moodle restore page after downloading the backup file in user draft area.';
$string['allowedmimetypes'] = 'Allowed mime types';
$string['allowedmimetypesdesc'] =
$string['allowedextensions'] = 'Allowed extensions';
$string['allowedextensionssdesc'] =
        'Provide a list of allowed mimetypes separated by ";". For example: 
        "pdf;docx;xlsx"';
$string['invalidtypes'] = 'The provided list contain invailed types';
$string['noantivirus'] = 'No antivirus installed! Please, protect yourself';
$string['virusfound'] = 'Virus: {$a} is infected!';
$string['moodlenet:allowsearch'] = 'View search page';
